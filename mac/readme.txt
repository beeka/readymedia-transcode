Change the /opt/local/sbin path in the plist file to the install location of minidlna then copy the file to /Library/LaunchDaemons to have the application load on startup.

It can be manually loaded/started with:

$ sudo launchctl load /Library/LaunchDaemons/net.sf.minidlna.plist
$ sudo launchctl start net.sf..minidlna

